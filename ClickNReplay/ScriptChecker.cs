﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickNReplay
{
	/// <summary>
	/// Helper class for checking a script syntax.
	/// </summary>
	/// author: Benjamin Billet
	/// version 0.1
	class ScriptChecker
	{
		private Script Script;
		private List<String> _errors = new List<String>();
		public List<String> Errors
		{
			get { return _errors; }
		}

		public String ErrorString
		{
			get { return String.Join(Environment.NewLine, _errors); }
		}

		public int ErrorCount
		{
			get { return _errors.Count; }
		}

		public ScriptChecker(Script script)
		{
			this.Script = script;
		}

		public Boolean CheckScript()
		{
			_errors.Clear();
			String[] instructions = Script.ScriptLines;
			for (int i = 0; i < instructions.Length; i++)
			{
				String[] elements = instructions[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				String keyword = elements[0];
				Console.WriteLine(instructions[i]);
				switch (keyword)
				{
					case Script.CLICKUP:
					case Script.CLICKDOWN:
					case Script.CLICK:
						if (elements.Length < 3 || elements.Length > 4)
							AddError(i, "Wrong param number");
						else if(IsUInt(elements[1]) == false || IsUInt(elements[2]) == false)
							AddError(i, "X and Y must be integers > 0");

						if (elements.Length == 4 && isOneOf(elements[3], Script.LEFT, Script.MIDDLE, Script.RIGHT) == false)
							AddError(i, String.Format("Mouse button must be one of {{{0}, {1}, {2}}}", Script.LEFT, Script.MIDDLE, Script.RIGHT));
						break;
					case Script.MOUSEMOVE:
						if (elements.Length == 3)
						{
							if (IsUInt(elements[1]) == false || IsUInt(elements[2]) == false)
								AddError(i, "X and Y must be integers > 0");
						}
						else if (elements.Length == 4)
						{
							if(Script.RELATIVE.Equals(elements[3]))
							{
								if (IsInt(elements[1]) == false || IsInt(elements[2]) == false)
									AddError(i, "X and Y must be integers");
							}
							else if (Script.ABSOLUTE.Equals(elements[3]))
							{
								if (IsUInt(elements[1]) == false || IsUInt(elements[2]) == false)
									AddError(i, "X and Y must be integers > 0");
							}
							else
								AddError(i, String.Format("Mode must be one of {{{0}, {1}}}", Script.RELATIVE, Script.ABSOLUTE));
						}
						else
							AddError(i, "Wrong param number");
						break;
					case Script.WHEEL:
						if (elements.Length == 3)
						{
							if (IsUInt(elements[1]) == false)
								AddError(i, "Number must be integer > 0");
							if(IsInt(elements[2]) == false)
								AddError(i, "Value must be integer");
						}
						else
							AddError(i, "Wrong param number");
						break;
					case Script.PAUSE:
						if (elements.Length == 2)
						{
							if (IsUInt(elements[1]) == false)
								AddError(i, "Time must be integer > 0");
						}
						else
							AddError(i, "Wrong param number");
						break;
					case Script.TYPE:
						if (elements.Length != 2)
							AddError(i, "Wrong param number");
						break;
					case Script.KEYDOWN:
					case Script.KEYUP:
					case Script.KEY:
						if (elements.Length == 2)
						{
							if (IsByte(elements[1]) == false)
								AddError(i, "KeyCode must be integer > 0");
						}
						else
							AddError(i, "Wrong param number");
						break;
					case Script.FOREGROUND:
						if (elements.Length != 2)
							AddError(i, "Wrong param number");
						break;
					default:
						AddError(i, "Unknown instruction");
						break;
				}
			}

			return _errors.Count == 0;
		}

		private void AddError(int line, String content)
		{
			_errors.Add(String.Format("line {0} : {1}", line + 1, content));
		}

		private Boolean IsInt(String value)
		{
			int someInt;
			return Int32.TryParse(value, out someInt);
		}

		private Boolean IsUInt(String value)
		{
			uint someUInt = 0;
			return UInt32.TryParse(value, out someUInt);
		}

		private Boolean IsByte(String value)
		{
			byte someByte = 0;
			return Byte.TryParse(value, out someByte);
		}

		private Boolean isOneOf(String value, params String[] values)
		{
			return values.Contains(value);
		}
	}
}
