﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClickNReplay
{
	/// <summary>
	/// A background worker for playing a script, with play/pause/resume/stop support.
	/// </summary>
	/// author: Benjamin Billet
	/// version 0.1
	class ScriptPlayer : BackgroundWorker
	{
		private Script Script;
		private Boolean _isPlaying = false;
		public Boolean IsPlaying
		{
			get { return _isPlaying; }
		}
		private Boolean _isPaused = false;
		public Boolean IsPaused
		{
			get { return _isPaused; }
		}
		private Boolean _loop = false;
		public Boolean Loop
		{
			get { return _loop; }
		}
		private int _timeBetweenInstructions = 1000;
		public int TimeBetweenInstructions
		{
			get { return _timeBetweenInstructions; }
		}

		// System Functions

		[DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
		public static extern void mouse_event(uint dwFlags, int dx, int dy, int dwData, uint dwExtraInfo);

		[DllImport("user32.dll")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		[DllImport("user32.dll", SetLastError = true)]
		public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

		// System constants

		public const int KEYEVENTF_EXTENDEDKEY = 0x0001; //Key down flag
		public const int KEYEVENTF_KEYUP = 0x0002; //Key up flag

		public const int MOUSEEVENTF_MOVE = 0x0001;
		public const int MOUSEEVENTF_LEFTDOWN = 0x0002;
		public const int MOUSEEVENTF_LEFTUP = 0x0004;
		public const int MOUSEEVENTF_RIGHTDOWN = 0x0008;
		public const int MOUSEEVENTF_RIGHTUP = 0x0010;
		public const int MOUSEEVENTF_MIDDLEDOWN = 0x0020;
		public const int MOUSEEVENTF_MIDDLEUP = 0x0040;
		public const int MOUSEEVENTF_ABSOLUTE = 0x8000;
		public const int MOUSEEVENTF_WHEEL = 0x0800;
		
		public ScriptPlayer(Script script, Boolean loop)
		{
			this.Script = script;
			this._loop = loop;
			this.WorkerSupportsCancellation = true;
			this.WorkerReportsProgress = true;
		}

		public void Play()
		{
			_isPlaying = true;
			RunWorkerAsync();
		}

		public void Pause()
		{
			_isPaused = true;
		}

		public void Resume()
		{
			_isPaused = false;
		}

		public void Stop()
		{
			_isPlaying = false;
			CancelAsync();
		}

		protected override void OnDoWork(DoWorkEventArgs e)
		{
			String[] instructions = Script.ScriptLines;
			if(instructions.Length > 0)
			{
				int currentInstruction = 0;
				int loopCounter = 1;
				while(_isPlaying)
				{
					if(CancellationPending)
					{ 
						e.Cancel = true;
						break; 
					}

					if(_isPaused == false)
					{
						ReportProgress(currentInstruction);
						Execute(instructions[currentInstruction], loopCounter);

						currentInstruction++;
						if (currentInstruction >= instructions.Length)
						{
							if(_loop)
							{
								currentInstruction = 0;
								loopCounter++;
							}
							else
								break;
						}
					}
					Thread.Sleep(_timeBetweenInstructions);
				}
			}

			_isPlaying = false;
			e.Result = null; 
		}

		// TODO refactor this function with a more structured interpreter
		private void Execute(String instruction, int loopCounter)
		{
			String[] elements = instruction.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			String keyword = elements[0];
			switch(keyword)
			{
				case Script.CLICKUP:
				case Script.CLICKDOWN:
				case Script.CLICK:
					Cursor.Position = new Point(Int32.Parse(elements[1]), Int32.Parse(elements[2]));
					Thread.Sleep(100);
					mouse_event(GetMouseEvent(elements), 0, 0, 0, 0);
					Thread.Sleep(100);
					break;
				case Script.MOUSEMOVE:
					if(elements.Length == 4 && elements[3].ToLower().Equals(Script.RELATIVE))
						mouse_event(MOUSEEVENTF_MOVE, Int32.Parse(elements[1]), Int32.Parse(elements[2]), 0, 0);
					else
						Cursor.Position = new Point(Int32.Parse(elements[1]), Int32.Parse(elements[2]));
					Thread.Sleep(100);
					break;
				case Script.WHEEL:
					int nb = Int32.Parse(elements[1]);
					int val = Int32.Parse(elements[2]);
					for(int i = 0; i < nb; i++)
					{
						mouse_event(MOUSEEVENTF_WHEEL, 0, 0, val, 0);
						Thread.Sleep(100);
					}
					break;
				case Script.PAUSE:
					Thread.Sleep(Int32.Parse(elements[1]));
					break;
				case Script.TYPE:
					String text = elements[1];
					if (_loop)
						text = text.Replace(Script.LOOP_COUNTER, loopCounter.ToString());
					text = EscapeText(text);
					SendKeys.SendWait(text);
					break;
				case Script.KEYDOWN:
					keybd_event(Byte.Parse(elements[1]), 0, KEYEVENTF_EXTENDEDKEY, 0);
					Thread.Sleep(100);
					break;
				case Script.KEYUP:
					keybd_event(Byte.Parse(elements[1]), 0, KEYEVENTF_KEYUP, 0);
					Thread.Sleep(100);
					break;
				case Script.KEY:
					keybd_event(Byte.Parse(elements[1]), 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
					Thread.Sleep(100);
					break;
				case Script.FOREGROUND:
					Process[] p = Process.GetProcessesByName(elements[1]);
					if (p.Count() > 0)
						SetForegroundWindow(p[0].MainWindowHandle);
					break;
			}
		}

		private String EscapeText(String text)
		{
			StringBuilder builder = new StringBuilder();
			char[] chars = text.ToCharArray();

			foreach(char c in chars)
			{
				switch(c)
				{
					case '+':
					case '%':
					case '~':
					case '^':
					case '[':
					case ']':
					case '{':
					case '}':
					case '(':
					case ')':
						builder.Append('{');
						builder.Append(c);
						builder.Append('}');
						break;

					default:
						builder.Append(c);
						break;
				}
			}

			return builder.ToString();
		}

		private uint GetMouseEvent(String[] elements)
		{
			uint up = MOUSEEVENTF_LEFTUP;
			uint down = MOUSEEVENTF_LEFTDOWN;

			if(elements.Length == 4)
			{ 
				if (elements[3].ToLower().Equals(Script.RIGHT))
				{
					up = MOUSEEVENTF_RIGHTUP;
					down = MOUSEEVENTF_RIGHTDOWN;
				}
				else if (elements[3].Equals(Script.MIDDLE))
				{
					up = MOUSEEVENTF_MIDDLEUP;
					down = MOUSEEVENTF_MIDDLEDOWN;
				}
			}

			uint evt = down | up;

			if (elements[0].Equals(Script.CLICKUP))
				evt = up;
			else if (elements[0].Equals(Script.CLICKDOWN))
				evt = down;

			return evt;
		}
	}
}
