﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClickNReplay
{
	/// <summary>
	/// Very simple script representation.
	/// </summary>
	/// author: Benjamin Billet
	/// version 0.1
	class Script
	{
		// Instructions
		public const String CLICKUP = "CLICKUP";
		public const String CLICKDOWN = "CLICKDOWN";
		public const String CLICK = "CLICK";
		public const String MOUSEMOVE = "MOUSEMOVE";
		public const String WHEEL = "WHEEL";
		public const String PAUSE = "PAUSE";
		public const String TYPE = "TYPE";
		public const String KEYDOWN = "KEYDOWN";
		public const String KEYUP = "KEYUP";
		public const String KEY = "KEY";
		public const String FOREGROUND = "FOREGROUND";

		// Keywords
		public const String RELATIVE = "relative";
		public const String ABSOLUTE = "absolute";
		public const String LEFT = "left";
		public const String MIDDLE = "middle";
		public const String RIGHT = "right";
		public const String LOOP_COUNTER = "[LOOP_COUNTER]";

		private int WheelCount = 0;
		private int WheelDirection = 0;
		private String TypedText = "";

		private List<String> ScriptContent = new List<String>();
		public String[] ScriptLines
		{
			get { return ScriptContent.ToArray(); }
		}

		public int Count
		{
			get { return ScriptContent.Count; }
		}

		public void AddClickEvent(MouseButtons button, int x, int y)
		{
			UpdateWheel();
			UpdateTyping();

			String type = "left";
			if (button == MouseButtons.Right)
				type = "right";
			else if (button == MouseButtons.Middle)
				type = "middle";

			AddEvent("CLICK", x.ToString(), y.ToString(), type);
		}

		public void AddWheelEvent(int delta)
		{
			UpdateTyping();

			if(delta == 0)
				return;

			if(WheelDirection != delta)
				UpdateWheel();

			if (WheelCount == 0)
				AddEvent("WHEEL", "<counting>", delta.ToString());

			WheelDirection = delta;
			WheelCount++;
		}

		public void AddKeyPressEvent(char character)
		{
			UpdateWheel();
			
			if(TypedText.Length > 0)
				ScriptContent.RemoveAt(ScriptContent.Count - 1);

			TypedText = TypedText + character;
			AddEvent("TYPE", TypedText);
		}

		private void AddEvent(String keyword, params String[] parameters)
		{
			ScriptContent.Add(keyword + " " + String.Join(" ", parameters));
		}

		private void UpdateWheel()
		{
			if (WheelCount > 0)
			{
				ScriptContent.RemoveAt(ScriptContent.Count - 1);
				AddEvent("WHEEL", WheelCount.ToString(), WheelDirection.ToString());
				WheelCount = 0;
			}
		}

		private void UpdateTyping()
		{
			if (TypedText.Length > 0)
			{
				ScriptContent.RemoveAt(ScriptContent.Count - 1);
				AddEvent("TYPE", TypedText);
				TypedText = "";
			}
		}

		public void Finish()
		{
			UpdateWheel();
			UpdateTyping();
		}

		public void RemoveAt(int index)
		{
			ScriptContent.RemoveAt(index);
		}

		public void Reset()
		{
			ScriptContent.Clear();
			WheelCount = 0;
			WheelDirection = 1;
			TypedText = "";
		}

		public void Parse(String content)
		{
			Reset();
			String[] lines = content.Split(Environment.NewLine.ToCharArray());
			foreach(String line in lines)
			{
				if(line.Trim().Length > 0)
					ScriptContent.Add(line);
			}
		}
	}
}
