﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClickNReplay
{
	/// author: Benjamin Billet
	/// version 0.1
    public partial class MainForm : Form
    {
		private UserActivityHook SystemEventHook;
		private ScriptPlayer ScriptPlayer;
		private Script Script = new Script();
		
		private Boolean IsRecording = false;
		private Boolean IsPlaying = false;
		private Boolean IsAltPressed = false;

		private String VersionNumber = "0.1";

        public MainForm()
        {
            InitializeComponent();
        }

		private void Play()
		{
			SynchronizeBack();

			ScriptChecker checker = new ScriptChecker(Script);
			if (checker.CheckScript() == false)
			{
				MessageBox.Show(checker.ErrorString, String.Format("There is {0} syntax error !", checker.ErrorCount), MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			PlayButton.Text = "Stop";
			SaveButton.Enabled = false;
			RecordButton.Enabled = false;
			LoadButton.Enabled = false;
			PauseButton.Visible = true;
			ScriptText.ReadOnly = true;
			IsPlaying = true;

			ScriptPlayer = new ScriptPlayer(Script, LoopCheckBox.Checked);
			ScriptPlayer.RunWorkerCompleted += PlayerFinished_Event;
			ScriptPlayer.ProgressChanged += PlayerUpdate_Event;
			ScriptPlayer.Play();
		}

		private void Pause()
		{
			PauseButton.Text = "Resume";
			ScriptPlayer.Pause();
		}

		private void Resume()
		{
			PauseButton.Text = "Pause";
			ScriptPlayer.Resume();
		}

		private void StopPlaying()
		{
			// force stop
			if (ScriptPlayer != null && ScriptPlayer.IsPlaying)
				ScriptPlayer.Stop();

			PlayButton.Text = "Play";
			SaveButton.Enabled = true;
			RecordButton.Enabled = true;
			LoadButton.Enabled = true;
			PauseButton.Visible = false;
			ScriptText.ReadOnly = false;
			IsPlaying = false;
			ScriptText.Select(0, 0); // put the caret at the beginning
		}

		private void Record()
		{
			SynchronizeBack();

			RecordButton.Text = "Stop Recording";
			SaveButton.Enabled = false;
			PlayButton.Enabled = false;
			LoadButton.Enabled = false;
			ScriptText.ReadOnly = true;
			IsRecording = true;
		}

		private void StopRecording()
		{
			Script.RemoveAt(Script.Count - 1); // remove the last click
			Script.Finish();
			Synchronize();

			RecordButton.Text = "Record";
			SaveButton.Enabled = true;
			PlayButton.Enabled = true;
			LoadButton.Enabled = true;
			ScriptText.ReadOnly = false;
			IsRecording = false;
		}
		
		/// <summary>
		/// Synchronize the script content with the script object.
		/// </summary>
		private void Synchronize()
		{
			ScriptText.Lines = Script.ScriptLines;

			// scroll to the end of the box
			ScriptText.SelectionStart = ScriptText.Text.Length;
			ScriptText.ScrollToCaret();
		}

		/// <summary>
		/// Synchronize the script object with the script content.
		/// </summary>
		private void SynchronizeBack()
		{
			Script.Parse(ScriptText.Text);
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			SystemEventHook = new UserActivityHook();
			SystemEventHook.OnMouseActivity += new MouseEventHandler(MouseMoved_Event);
			SystemEventHook.KeyDown += new KeyEventHandler(KeyDown_Event);
			SystemEventHook.KeyPress += new KeyPressEventHandler(KeyPress_Event);
			SystemEventHook.KeyUp += new KeyEventHandler(KeyUp_Event);
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if(IsRecording)
				StopRecording();

			if(IsPlaying)
				StopPlaying();
		}

		private void MouseMoved_Event(object sender, MouseEventArgs e)
		{
			MouseLocationLabel.Text = String.Format("x={0} y={1}", e.X, e.Y);
			if (IsRecording)
			{
				if(e.Delta != 0)
					Script.AddWheelEvent(e.Delta);
			
				if(e.Clicks > 0)
					Script.AddClickEvent(e.Button, e.X, e.Y);

				Synchronize();
			}
		}

		private void KeyPress_Event(object sender, KeyPressEventArgs e)
		{
			if (IsRecording)
			{
				Script.AddKeyPressEvent(e.KeyChar);
				Synchronize();
			}
		}

		private void KeyDown_Event(object sender, KeyEventArgs e)
		{
			KeyCodeLabel.Text = String.Format("{0} ({1} 0x{2})", e.KeyCode, e.KeyValue, e.KeyValue.ToString("x").ToUpper());
			if (e.KeyCode == Keys.Menu || e.KeyCode == Keys.LMenu || e.KeyCode == Keys.RMenu)
				IsAltPressed = true;
		}

		private void KeyUp_Event(object sender, KeyEventArgs e)
		{
			KeyCodeLabel.Text = "<waiting>";
			if (e.KeyCode == Keys.Menu || e.KeyCode == Keys.LMenu || e.KeyCode == Keys.RMenu)
				IsAltPressed = false;
			else if (IsPlaying && IsAltPressed)
			{
				if (e.KeyCode == Keys.S)
					ScriptPlayer.Stop();
				else if (e.KeyCode == Keys.R)
					ScriptPlayer.Resume();
				else if (e.KeyCode == Keys.P)
					ScriptPlayer.Pause();
			}
		}

		private void PlayButton_Click(object sender, EventArgs e)
		{
			if (IsPlaying)
				StopPlaying();
			else
				Play();
		}

		private void PauseButton_Click(object sender, EventArgs e)
		{
			if(ScriptPlayer.IsPaused)
				Resume();
			else
				Play();
		}

		private void PlayerFinished_Event(object sender, RunWorkerCompletedEventArgs e)
		{
			StopPlaying();
		}

		private void PlayerUpdate_Event(object sender, ProgressChangedEventArgs e)
		{
			if (IsPlaying)
			{
				ScriptText.Focus();
				int position = ScriptText.GetFirstCharIndexFromLine(e.ProgressPercentage);
				int lineEnd = ScriptText.Text.IndexOf(Environment.NewLine, position);
				
				if(lineEnd < 0) // end of the box
					lineEnd = ScriptText.Text.Length;

				// select the currently played instruction
				if(position >= 0 && lineEnd > position)
					ScriptText.Select(position, lineEnd - position);
			}
		}

		private void RecordButton_Click(object sender, EventArgs e)
		{
			if(IsRecording)
				StopRecording();
			else
				Record();
		}

		private void LoadButton_Click(object sender, EventArgs e)
		{
			String path = null;
			OpenFileDialog dialog = new OpenFileDialog();
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				path = dialog.FileName;
				try
				{
					String content = File.ReadAllText(path, Encoding.UTF8);
					ScriptText.Text = content;
					SynchronizeBack();
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "An exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			String path = null;
			SaveFileDialog dialog = new SaveFileDialog();
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				path = dialog.FileName;
				try
				{
					File.WriteAllText(path, ScriptText.Text, Encoding.UTF8);
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "An exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private void ScriptText_KeyDown(object sender, KeyEventArgs e)
		{
			// emulate Ctrl+A
			if (ScriptText.ReadOnly == false && ScriptText.Enabled && e.Control && e.KeyCode == Keys.A)
				ScriptText.SelectAll();

			// disable Ctrl+Z because of unwanted behavior
			if (e.Control && e.KeyCode == Keys.Z)
			{ 
				e.Handled = true;
				e.SuppressKeyPress = true;
			}
		}

		/// <summary>
		/// Display the informations about the application.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void AboutLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			String message = "Click n' Replay " + VersionNumber + ", written by Benjamin Billet." + Environment.NewLine
								+ "Thanks for using this application, please report any bug on the project page." + Environment.NewLine + Environment.NewLine
								+ "http://benjaminbillet.fr" + Environment.NewLine
								+ "https://bitbucket.org/bbillet/clicknreplay" + Environment.NewLine + Environment.NewLine
								+ "This is a non commercial application, without warranty of any kind. " + Environment.NewLine
								+ "Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)";
			MessageBox.Show(this, message, "About Click n' Play", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
    }
}
