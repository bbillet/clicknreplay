﻿namespace ClickNReplay
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.ScriptText = new System.Windows.Forms.TextBox();
			this.LoadButton = new System.Windows.Forms.Button();
			this.SaveButton = new System.Windows.Forms.Button();
			this.PlayButton = new System.Windows.Forms.Button();
			this.RecordButton = new System.Windows.Forms.Button();
			this.PauseButton = new System.Windows.Forms.Button();
			this.StatusBar = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.MouseLocationLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.KeyCodeLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.LoopCheckBox = new System.Windows.Forms.CheckBox();
			this.AboutLink = new System.Windows.Forms.LinkLabel();
			this.StatusBar.SuspendLayout();
			this.SuspendLayout();
			// 
			// ScriptText
			// 
			this.ScriptText.Location = new System.Drawing.Point(12, 60);
			this.ScriptText.Multiline = true;
			this.ScriptText.Name = "ScriptText";
			this.ScriptText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.ScriptText.Size = new System.Drawing.Size(466, 317);
			this.ScriptText.TabIndex = 2;
			this.ScriptText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScriptText_KeyDown);
			// 
			// LoadButton
			// 
			this.LoadButton.Location = new System.Drawing.Point(403, 12);
			this.LoadButton.Name = "LoadButton";
			this.LoadButton.Size = new System.Drawing.Size(75, 23);
			this.LoadButton.TabIndex = 3;
			this.LoadButton.Text = "Load";
			this.LoadButton.UseVisualStyleBackColor = true;
			this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
			// 
			// SaveButton
			// 
			this.SaveButton.Location = new System.Drawing.Point(322, 12);
			this.SaveButton.Name = "SaveButton";
			this.SaveButton.Size = new System.Drawing.Size(75, 23);
			this.SaveButton.TabIndex = 4;
			this.SaveButton.Text = "Save";
			this.SaveButton.UseVisualStyleBackColor = true;
			this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
			// 
			// PlayButton
			// 
			this.PlayButton.Location = new System.Drawing.Point(12, 12);
			this.PlayButton.Name = "PlayButton";
			this.PlayButton.Size = new System.Drawing.Size(75, 23);
			this.PlayButton.TabIndex = 5;
			this.PlayButton.Text = "Play";
			this.PlayButton.UseVisualStyleBackColor = true;
			this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
			// 
			// RecordButton
			// 
			this.RecordButton.Location = new System.Drawing.Point(241, 12);
			this.RecordButton.Name = "RecordButton";
			this.RecordButton.Size = new System.Drawing.Size(75, 23);
			this.RecordButton.TabIndex = 7;
			this.RecordButton.Text = "Record";
			this.RecordButton.UseVisualStyleBackColor = true;
			this.RecordButton.Click += new System.EventHandler(this.RecordButton_Click);
			// 
			// PauseButton
			// 
			this.PauseButton.Location = new System.Drawing.Point(93, 12);
			this.PauseButton.Name = "PauseButton";
			this.PauseButton.Size = new System.Drawing.Size(75, 23);
			this.PauseButton.TabIndex = 8;
			this.PauseButton.Text = "Pause";
			this.PauseButton.UseVisualStyleBackColor = true;
			this.PauseButton.Visible = false;
			this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
			// 
			// StatusBar
			// 
			this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.MouseLocationLabel,
            this.toolStripStatusLabel2,
            this.KeyCodeLabel});
			this.StatusBar.Location = new System.Drawing.Point(0, 390);
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Size = new System.Drawing.Size(490, 22);
			this.StatusBar.TabIndex = 9;
			this.StatusBar.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(95, 17);
			this.toolStripStatusLabel1.Text = "Mouse Location:";
			// 
			// MouseLocationLabel
			// 
			this.MouseLocationLabel.Name = "MouseLocationLabel";
			this.MouseLocationLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.MouseLocationLabel.Size = new System.Drawing.Size(62, 17);
			this.MouseLocationLabel.Text = "<waiting>";
			// 
			// toolStripStatusLabel2
			// 
			this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
			this.toolStripStatusLabel2.Size = new System.Drawing.Size(29, 17);
			this.toolStripStatusLabel2.Text = "Key:";
			// 
			// KeyCodeLabel
			// 
			this.KeyCodeLabel.Name = "KeyCodeLabel";
			this.KeyCodeLabel.Size = new System.Drawing.Size(62, 17);
			this.KeyCodeLabel.Text = "<waiting>";
			// 
			// LoopCheckBox
			// 
			this.LoopCheckBox.AutoSize = true;
			this.LoopCheckBox.Location = new System.Drawing.Point(15, 39);
			this.LoopCheckBox.Name = "LoopCheckBox";
			this.LoopCheckBox.Size = new System.Drawing.Size(50, 17);
			this.LoopCheckBox.TabIndex = 10;
			this.LoopCheckBox.Text = "Loop";
			this.LoopCheckBox.UseVisualStyleBackColor = true;
			// 
			// AboutLink
			// 
			this.AboutLink.AutoSize = true;
			this.AboutLink.Location = new System.Drawing.Point(381, 394);
			this.AboutLink.Name = "AboutLink";
			this.AboutLink.Size = new System.Drawing.Size(97, 13);
			this.AboutLink.TabIndex = 11;
			this.AboutLink.TabStop = true;
			this.AboutLink.Text = "About this software";
			this.AboutLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.AboutLink_LinkClicked);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(490, 412);
			this.Controls.Add(this.AboutLink);
			this.Controls.Add(this.LoopCheckBox);
			this.Controls.Add(this.StatusBar);
			this.Controls.Add(this.PauseButton);
			this.Controls.Add(this.RecordButton);
			this.Controls.Add(this.PlayButton);
			this.Controls.Add(this.SaveButton);
			this.Controls.Add(this.LoadButton);
			this.Controls.Add(this.ScriptText);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Click n\' Replay";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.StatusBar.ResumeLayout(false);
			this.StatusBar.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.TextBox ScriptText;
		private System.Windows.Forms.Button LoadButton;
		private System.Windows.Forms.Button SaveButton;
		private System.Windows.Forms.Button PlayButton;
		private System.Windows.Forms.Button RecordButton;
		private System.Windows.Forms.Button PauseButton;
		private System.Windows.Forms.StatusStrip StatusBar;
		private System.Windows.Forms.ToolStripStatusLabel MouseLocationLabel;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.CheckBox LoopCheckBox;
		private System.Windows.Forms.LinkLabel AboutLink;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
		private System.Windows.Forms.ToolStripStatusLabel KeyCodeLabel;


    }
}

